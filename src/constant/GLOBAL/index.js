export const HOST = 'http://localhost:1337';
export const API = {
    "login": "/login",
    "register": "/register",
    "listTodo": "/list-todo",
    "createTodo": "/create-todo",
    "updateTodo": "/update-todo",
    "deleteTodo": "/delete-todo"
}