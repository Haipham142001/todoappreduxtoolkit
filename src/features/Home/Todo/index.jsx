import React from 'react';
import { useDispatch } from 'react-redux';
import { deleteTodo } from '../../../redux/TodoSlice';
import fetchApi from '../../../services/api';
import { API } from '../../../constant/GLOBAL';
import Notification from '../../../services/Notification';

const Todo = (props) => {
  const todo = props.data;
  const dispatch = useDispatch();

  const handleDeleteTodo = async (index, id) => {
    const value = await fetchApi(API.deleteTodo, "POST", { id: id });
    if (value.message) {
      if (value.message.indexOf('hoàn tất')) {
        dispatch(deleteTodo(index));
        Notification('Thông báo', value.message, 'success');
      } else {
        Notification('Thông báo', value.message, 'error');
      }
      if (value.token) {
        localStorage.setItem('authKey', value.token);
      }
    }
  }

  const handleUpdateTodo = (todo) => {
    props.onUpdate(todo);
  }

  return (
    <tr>
      <td>{props.stt + 1}</td>
      <td>{todo.name}</td>
      <td>{todo.dateStart}</td>
      <td>{todo.deadline}</td>
      <td>{todo.status === 0
        ? <input type='checkbox' />
        : <input type='checkbox' defaultChecked />}</td>
      <td>
        <button onClick={() => handleUpdateTodo(todo)}>Sửa</button>
        <button onClick={() => handleDeleteTodo(props.stt, todo.id)}>Xóa</button>
        <div id="toast"></div>
      </td>
    </tr>
  )
}

export default Todo