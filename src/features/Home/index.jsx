import React, { useEffect, useState } from 'react';
import Header from '../components/Header';
import { useSelector, useDispatch } from 'react-redux';
import { addTodo, listTodo, updateTodo } from '../../redux/TodoSlice.js';
import fetchApi from '../../services/api.js';
import { API } from '../../constant/GLOBAL';
import ListTodo from './ListTodo';
import Notification from '../../services/Notification';

const Home = () => {

    const store = useSelector(state => state.todos);
    const [name, setName] = useState('');
    const [dateStart, setDateStart] = useState('');
    const [deadline, setDeadline] = useState('');
    const [isUpdate, setIsUpdate] = useState(false);
    const [todoUpdate, setTodoUpdate] = useState({});
    const dispatch = useDispatch();

    useEffect(() => {
        async function a() {
            const data = await fetchApi(API.listTodo, "POST", {});
            if (data.token) {
                localStorage.setItem('authKey', data.token);
            }
            dispatch(listTodo(data.todos));
        }
        a();
    }, [dispatch])

    const hanldeAddTodo = async () => {
        if (name !== '' && dateStart !== '' && deadline !== '') {
            const data = {
                name: name,
                deadline: deadline,
                dateStart: dateStart,
                status: false
            }
            let value;
            if(isUpdate !== true) {
                value = await fetchApi(API.createTodo, "POST", data);
            } else {
                console.log('ok');
                data.id = todoUpdate.id;
                value = await fetchApi(API.updateTodo, "POST", data);
            }
            console.log(data);
            if (value.message) {
                if (value.message.indexOf('hoàn tất')) {
                    Notification('Thông báo', value.message, 'success');
                    if(isUpdate !== true) {
                        dispatch(addTodo(data));
                    } else {
                        dispatch(updateTodo(data));
                        setIsUpdate(false);
                    }
                    setName('');
                    setDeadline('');
                    setDateStart('');
                } else {
                    Notification('Thông báo', value.message, 'error');
                }
                if (value.token) {
                    localStorage.setItem('authKey', value.token);
                }
            }
        } else {
            if (name === '') {
                Notification('Thông báo', 'Thiếu name', 'error');
            };
            if (deadline === '') {
                Notification('Thông báo', 'Thiếu deadline', 'error');
            }
            if (dateStart === '') {
                Notification('Thông báo', 'Thiếu dateStart', 'error');
            }
        }
    }

    const onHanldeUpdateTodo = (todo) => {
        setName(todo.name);
        setDateStart(todo.dateStart);
        setDeadline(todo.deadline);
        setIsUpdate(true);
        setTodoUpdate(todo);
    }

    return (
        <div>
            <Header />
            <div id="toast"></div>
            <div className='login-form'>
                <h1>{isUpdate === true ? 'Update' : 'Add'}</h1>
                <input type='text' placeholder='name' value={name} onChange={(e) => setName(e.target.value)} />
                <input placeholder='date start' value={dateStart} onChange={(e) => setDateStart(e.target.value)} />
                <input type="text" placeholder='deadline' value={deadline} onChange={(e) => setDeadline(e.target.value)} />
                <br />
                <button onClick={hanldeAddTodo}>
                    {isUpdate === true ? 'Update' : 'Add'}
                </button>
            </div>
            <table border={1}>
                <thead>
                    <tr>
                        <th>
                            STT
                        </th>
                        <th>
                            Name
                        </th>
                        <th>
                            Date start
                        </th>
                        <th>
                            Deadline
                        </th>
                        <th>
                            Status
                        </th>
                        <th>
                            Action
                        </th>
                    </tr>
                </thead>
                <ListTodo todos={store} onUpdate={(e) => onHanldeUpdateTodo(e)} />
            </table>
        </div>
    )
}

export default Home