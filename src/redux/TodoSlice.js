import { createSlice } from '@reduxjs/toolkit'

export const TodoSlice = createSlice({
    name: 'todo',
    initialState: [],
    reducers: {
        addTodo: (state, action) => {
            state.push(action.payload);
            return state;
        },
        listTodo: (state, action) => {
            state = action.payload;
            return state;
        },
        deleteTodo: (state, action) => {
            state.splice(action.payload, 1);
            return state;
        },
        updateTodo: (state, action) => {
            const index = state.findIndex((todo) => {
                return todo.id === action.payload.id;
            })
            state[index] = action.payload;
            return state;
        }
    },
})

// Action creators are generated for each case reducer function
export const { addTodo, listTodo, deleteTodo, updateTodo } = TodoSlice.actions

export default TodoSlice.reducer