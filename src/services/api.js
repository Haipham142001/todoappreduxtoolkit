import { HOST } from "../constant/GLOBAL";

const fetchApi = async (url, method, body) => {
    const response = await fetch(HOST + url, {
        method: method,
        headers: {
            'Content-Type': 'application/json',
            "Authorization": 'Bearer ' + localStorage.getItem("authKey")
        },
        body: JSON.stringify(body)
    })
    const result = await response.json();
    return result;
}

export default fetchApi